import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  const container = render(<App />);
  //const content = screen.getByText(/Martin Fowler/i);
  const content = screen.getByText(/Martin Fowler/i);
//  expect(container).toHaveClass('App')
  expect(content).toBeInTheDocument();
});
