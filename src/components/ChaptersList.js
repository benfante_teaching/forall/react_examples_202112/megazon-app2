const ChaptersList = ({ chapters }) => (
  <ul>
    {chapters.map((chapter, i) => (
      <li key={i}>{chapter}</li>
    ))}
  </ul>
);
export default ChaptersList