import { useInput } from "../hooks/useInput"

const AddCommentForm = ({onSubmit = f => f}) => {
    
    const [nameInput, nameReset] = useInput('')
    const [textInput, textReset] = useInput('')

    const submit = (e) => {
        e.preventDefault()
        onSubmit(nameInput.value, textInput.value)
        nameReset()
        textReset()
    }

    return (
        <form onSubmit={submit}>
            <div>
                <label htmlFor="nameInput">Name:</label>
                <input id="nameInput" type="text" {...nameInput} />
            </div>
            <div>
                <label htmlFor="textInput">Comment:</label>
                <textarea id="textInput" {...textInput} />
            </div>
            <div><button>Add</button></div>
        </form>
    )
}

export default AddCommentForm