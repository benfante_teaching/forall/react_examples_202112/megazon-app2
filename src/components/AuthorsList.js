import React from "react"

const AuthorsList = ({authors}) => {
    return (
      <React.Fragment>
          <h3>Authors</h3>
          <ul>
              {authors.map((author, i) => <li key={i}>{author}</li>)}
          </ul>
      </React.Fragment>
    )
}

export default AuthorsList