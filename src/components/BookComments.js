import React, { useState } from "react";
import AddCommentForm from "./AddCommentForm";

const BookComments = ({ comments = [], onAddComment = (f) => f }) => {
  const [isShowing, setIsShowing] = useState(false);
  const [currentComments, setCurrentComments] = useState([...comments]);

  const toggleComments = () => {
    setIsShowing((b) => !b);
  };

  const addComment = (user, text) => {
    const comment = { date: new Date(), user, text };
    setCurrentComments([...currentComments, comment]);
    onAddComment(comment);
  };

  return !isShowing ? (
    <button onClick={toggleComments}>Show Comments</button>
  ) : (
    <React.Fragment>
      <button onClick={toggleComments}>Hide Comments</button>
      <table>
        <tbody>
          {currentComments.map((comment, i) => (
            <tr key={i}>
              <td>{comment.date.toLocaleString()}</td>
              <td>{comment.user}</td>
              <td>{comment.text}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <AddCommentForm onSubmit={addComment} />
    </React.Fragment>
  );
};

export default BookComments;
