import React from "react";
import AuthorsList from "./AuthorsList";
import BookComments from "./BookComments";
import ChaptersList from "./ChaptersList";

const Book = ({ book, onAddComment }) => (
  <React.Fragment>
    <h1>{book.title}</h1>
    <h2>{book.subtitle}</h2>
    <AuthorsList authors={book.authors} />
    <ChaptersList chapters={book.chapters} />
    <BookComments comments={book.comments} onAddComment={onAddComment}/>
  </React.Fragment>
);

export default Book