import books from "../data/books.json"

export const getBooks = (search) => {
    return new Promise((resolve, reject) => {
        if (!books) {
          setTimeout(() => reject(new Error('No books database')), Math.random() * 1000);
          return;
        }
        setTimeout(() => resolve(books), Math.random() * 1000)
      })
}