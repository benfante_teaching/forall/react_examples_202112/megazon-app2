import { useEffect, useState } from "react";
import "./App.css";
import Book from "./components/Book";
import { useInput } from "./hooks/useInput";
import { getBooks } from "./services/dataServices";

function App() {
  const [book, setBook] = useState();
  const [search] = useInput('react+javascript');

  useEffect(() => {
    getBooks(search.value)
      // .then((res) => res.json())
      .then((archive) => archive.items)
      .then((items) => items[0])
      .then((item) => item.volumeInfo)
      .then(({ title, subtitle = '', authors = [], chapters = [], comments = [] }) => {
        return { title, subtitle, authors, chapters, comments };
      })
      .then((book) => setBook(book))
      .catch(console.error);
  }, [search]);

  // let book = {
  //   title: "UML Distilled",
  //   subtitle: "Applying the standard Object Modeling Language",
  //   authors: ["Martin Fowler", "Kendal Scott"],
  //   chapters: [
  //     "Introduction",
  //     "An Outline Development Process",
  //     "Use Cases",
  //     "Class Diagrams: The Essentials",
  //     "Class Diagrams: Advanced Concepts",
  //     "Interaction Diagrams",
  //   ],
  //   comments: [
  //     {date: new Date(), user: 'Lucio Benfante', text: 'A first comment!'}
  //   ]
  // };

  const addComment = (comment) => {
    book.comments.push(comment);
    // and maybe a call to a REST service
  };

  return (
    <div className="App">
      <header className="App-header">
        Megazon App
      </header>
      <form onSubmit={e => e.preventDefault()}>
        <input placeholder="Search the best first book..." {...search}/>
      </form>
      {!book ? (
          <div>Loading books...</div>
        ) : (
          <Book book={book} onAddComment={(comment) => addComment(comment)} />
        )}
    </div>
  );
}

export default App;
